// Import React
import React from 'react'

// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Deck,
  Heading,
  Image,
  ListItem,
  List,
  Quote,
  Slide
} from 'spectacle'

// Import theme
import createTheme from 'spectacle/lib/themes/default'

import logo from '../assets/logo.png'
import fullbrightInitial from '../assets/fullbrightInitial.png'

// Require CSS
require('normalize.css')

const theme = createTheme({
  primary: '#2D3D57',
  secondary: '#FFFBB0',
  tertiary: '#ED9491',
  quarternary: '#FFF7ED',
  brand: '#01DAC5',
  brandLight: '#feffff'
}, {
  primary: {name: 'Overpass Mono', googleFont: true, styles: ['400']},
  secondary: {name: 'Overpass', googleFont: true, styles: ['400']}
})

export default class Presentation extends React.Component {
  render () {
    return (
      <Deck transition={['fade', 'slide']} transitionDuration={300} theme={theme} progress='bar'>
        <Slide transition={['fade']} bgColor='primary'>
          <Heading size={1} fit caps lineHeight={1} textColor='secondary'>
            quicksave
          </Heading>
          <br />
          <hr />
          <Image src={logo} alt='multi-cell logo' width={120} />
        </Slide>
        <Slide transition={['fade']} bgColor='quarternary'>
          <Heading size={5} textColor='primary'>Fullbright in 2012</Heading>
          <Image src={fullbrightInitial} alt='Fullbright organization diagram as of 2012' fit />
        </Slide>
        <Slide transition={['fade']} bgColor='primary' textColor='tertiary'>
          <Heading size={6} textColor='secondary' caps>Standard List</Heading>
          <List>
            <ListItem>Item 1</ListItem>
            <ListItem>Item 2</ListItem>
            <ListItem>Item 3</ListItem>
            <ListItem>Item 4</ListItem>
          </List>
        </Slide>
        <Slide transition={['fade']} bgColor='secondary' textColor='primary'>
          <BlockQuote>
            <Quote>Example Quote</Quote>
            <Cite>Author</Cite>
          </BlockQuote>
        </Slide>
      </Deck>
    )
  }
}
